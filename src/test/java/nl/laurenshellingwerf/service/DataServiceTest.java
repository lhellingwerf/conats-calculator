package nl.laurenshellingwerf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import org.junit.Test;

public class DataServiceTest {
	
	private DataServiceImpl dataService = new DataServiceImpl();

	@Test
	public void testGetPredictedProbability() {
		int age = 74;
		int he4 = 150;
		int WHOPerformanceScore = 0;
		double predictedProbability = dataService.getPredictedProbability(age, he4, WHOPerformanceScore);
		
		System.err.println("het resultaat is: " + predictedProbability);
	}

	@Test
	public void testLoadSpecificityValues() {
//	    Map<Double, BigDecimal> specificityMap = dataService.getSpecificityMap();
//	    
//	    for (Double key : specificityMap.keySet()) {
//		String message = String.format("key is: %s, value is: %s", Double.toString(key), specificityMap.get(key).toString());
//		System.err.println(message);
//	    }
//	    
//	    String joe = String.format("value for key 0.45 is: %s",  specificityMap.get(0.45d).toString());
//	    System.out.println(joe);
	}
	
	
	@Test
	public void testBigDecimal() {
		BigDecimal result = new BigDecimal("72.4999");
		result = result.setScale(0, RoundingMode.HALF_UP);
		System.out.println(result);
	}
}