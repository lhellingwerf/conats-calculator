package nl.laurenshellingwerf.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import nl.laurenshellingwerf.domain.ConatsResult;
import nl.laurenshellingwerf.service.DataService;

//@Stateless
//@Path("/test")
@Path("/conats")
public class RestService {
    
    @Inject
    private DataService dataService;

    @GET
    @Path("/{age}/{performanceScoreWHO}/{he4}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTestmessage(@PathParam("age") int age, @PathParam("performanceScoreWHO") int performanceScoreWHO, @PathParam("he4") int he4) {
	
	ConatsResult result = getConatsResult(age, performanceScoreWHO, he4);
	
	return Response.ok(result).build();
    }

    private ConatsResult getConatsResult(int age, int performanceScoreWHO, int he4) {
	double score  = dataService.getPredictedProbability(age, he4, performanceScoreWHO);
	int specificity = dataService.getSpecificity(score);
	int negativePredictiveValue = dataService.getNegativePredictiveValue(score);
	return new ConatsResult(score, specificity, negativePredictiveValue);
    }

}
