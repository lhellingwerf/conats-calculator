package nl.laurenshellingwerf.domain;

public class ConatsResult {

    private double score;

    private int specificity;

    private int negativePredictiveValue;
    
    public ConatsResult(double score, int completeDebulking, int incompleteDebulking) {
	this.score = score;
	this.specificity = completeDebulking;
	this.negativePredictiveValue = incompleteDebulking;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getCompleteDebulking() {
        return specificity;
    }

    public void setCompleteDebulking(int completeDebulking) {
        this.specificity = completeDebulking;
    }

    public int getIncompleteDebulking() {
        return negativePredictiveValue;
    }

    public void setIncompleteDebulking(int incompleteDebulking) {
        this.negativePredictiveValue = incompleteDebulking;
    }
}
