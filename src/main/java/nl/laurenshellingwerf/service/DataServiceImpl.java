package nl.laurenshellingwerf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import javax.ejb.Stateless;

import nl.laurenshellingwerf.repository.ConatsRepository;

@Stateless
public class DataServiceImpl implements DataService {
    
    private static final double CONSTANT_VALUE = -8.512;
    
    @Override
    public double getPredictedProbability(int age, int he4, int whoPerformanceScore) {
	double conatsScore = calculateConatsScore(age, he4, whoPerformanceScore);
	//return Math.exp(conatsScore) / (1 + Math.exp(conatsScore));
	double predictedProbability = Math.exp(conatsScore) / (1 + Math.exp(conatsScore));
        BigDecimal roundedPredictedProbability = BigDecimal.valueOf(predictedProbability);
	roundedPredictedProbability = roundedPredictedProbability.setScale(2, RoundingMode.HALF_UP);
	
	return roundedPredictedProbability.doubleValue();	
    }

    /**
     * - 8.512 + 0.428 * age/10 + 0.648 * log2(HE4) + PS
     * 
     * age > 0 he4 > 0 0 =< performanceScore >= 3
     * 
     * @param age
     * @param factor
     * @return
     */
    private double calculateConatsScore(int age, int he4, int WHOPerformanceScore) {
	double ageFactor = getAgeFactor(age);
	double he4Factor = getHE4Factor(he4);
	double performanceScore = getPerformanceScore(WHOPerformanceScore);
	return CONSTANT_VALUE + ageFactor / 10 + he4Factor + performanceScore;
    }


    @Override
    public int getSpecificity(double conatsScore) {
	Map<Double, BigDecimal> specificityMap = ConatsRepository.getSpecificityMap();
	BigDecimal specificity = specificityMap.get(conatsScore);
	return specificity.intValue();
    }

    @Override
    public int getNegativePredictiveValue(double conatsScore) {
	Map<Double, BigDecimal> negativePredictiveValueMap = ConatsRepository.getNegativePredictiveValueMap();
	BigDecimal negativePredictiveValue = negativePredictiveValueMap.get(conatsScore);
	return negativePredictiveValue.intValue();
    }
    
    private double getAgeFactor(int age) {
	return 0.428 * age;
    }

    private double getHE4Factor(int he4) {
	return 0.648 * log2(he4);
    }

    private double log2(double value) {
	return Math.log(value) / Math.log(2);
    }

    private double getPerformanceScore(int whoPerformanceScore) {
	double result;
	switch (whoPerformanceScore) {
	case 0:
	    result = 0;
	    break;
	case 1:
	    result = 1.673;
	    break;
	case 2:
	    result = 1.904;
	    break;
	case 3:
	    result = 1.904;
	    break;
	default:
	    result = 0;
	    break;
	}
	return result;
    }

}
