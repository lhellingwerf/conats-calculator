package nl.laurenshellingwerf.service;

public interface DataService {

	double getPredictedProbability(int age, int he4, int WHOPerformanceScore);

	int getSpecificity(double conatsScore);
	
	int getNegativePredictiveValue(double conatsScore);
}
