package nl.laurenshellingwerf.repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class ConatsRepository {

    private static final String SPECIFICITY_TABLE_LOCATION = "/data/specificityTable.csv";

    // TODO: klopt de eerste toevoeging voor CONATS 0.0: 0.0;17,2??
    private static final String NEGATIVE_PREDICTIVE_VALUE_TABLE_LOCATION = "/data/negativePredictiveValueTable.csv";

    private static Map<Double, BigDecimal> specificityMap;
    
    private static Map<Double, BigDecimal> negativePredictiveValueMap;

    // TODO: is het wel zo'n goed idee om elke keer de hele map terug te geven? Niet beter hier de waarde er al uit vissen?
    public static Map<Double, BigDecimal> getSpecificityMap() {
	if (specificityMap == null) {
	    fillSpecificityMap();
	}
	return specificityMap;
    }
    
    public static Map<Double, BigDecimal> getNegativePredictiveValueMap() {
	if (negativePredictiveValueMap == null) {
	    fillNegativePredictiveValueMap();
	}
	return negativePredictiveValueMap;
    }    
    
    private static void fillSpecificityMap() {
	specificityMap = new HashMap<>();

	try (InputStream dataStream = ConatsRepository.class.getResourceAsStream("/data/specificityTable.csv")) {
//	    String dataString = IOUtils.toString(dataStream, StandardCharsets.UTF_8.name());
	    Stream<String> lines =  new BufferedReader(new InputStreamReader(dataStream)).lines();
	    lines.forEach(line -> insertLineInSpecificityMap(line));
	    lines.close();
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	
//	try {
	    //URL systemResource = ClassLoader.getSystemResource("/data/specificityTable.csv");
//	    URL resource = ConatsRepository.class.getClassLoader().getResource("specificityTable.csv");
//	    URI uri = resource.toURI();
//	    Path path = Paths.get(uri);
//	    Stream<String> lines = Files.lines(path);
//	    lines.forEach(line -> insertLineInSpecificityMap(line));
//	    lines.close();
	    
//	    URL resource = ConatsRepository.class.getResource("/data/specificityTable.csv");
//	    URI uri = resource.toURI();
//	    Path path = Paths.get(uri);
//	    Stream<String> lines = Files.lines(path);
//	    lines.forEach(line -> insertLineInSpecificityMap(line));
//	    lines.close();		
//	} catch (IOException | URISyntaxException e) {
	    // TODO Auto-generated catch block
//	    e.printStackTrace();
//	}
    }

    private static void insertLineInSpecificityMap(String line) {
	String lineArray[] = line.split(";");
	specificityMap.put(Double.parseDouble(lineArray[0]), getRoundedBigDecimal(lineArray[1]));
    }
    
    private static void fillNegativePredictiveValueMap() {
	negativePredictiveValueMap = new HashMap<>();

	try (InputStream dataStream = ConatsRepository.class.getResourceAsStream(NEGATIVE_PREDICTIVE_VALUE_TABLE_LOCATION)) {
	    Stream<String> lines =  new BufferedReader(new InputStreamReader(dataStream)).lines();
	    lines.forEach(line -> insertLineInNegativePredictiveValueMap(line));
	    lines.close();
	} catch (IOException e1) {
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
	
    }
    
    private static void insertLineInNegativePredictiveValueMap(String line) {
	String lineArray[] = line.split(";");
	negativePredictiveValueMap.put(Double.parseDouble(lineArray[0]), getRoundedBigDecimal(lineArray[1]));
    }
    
    private static BigDecimal getRoundedBigDecimal(String value) {
	BigDecimal result = new BigDecimal(value);
	result = result.setScale(0, RoundingMode.HALF_UP);
	return result;
    }
}
